/*
    ./webpack.config.js
*/
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({template: './client/index.html', filename: 'index.html', inject: 'body'})

module.exports = {
  entry: './client/index.js',
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js',
		publicPath: '/'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }, {
        test: /\.jsx$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }, {
        test: /\.scss/,
        loader: "style-loader!css-loader!sass-loader"
      }, {
        test: /\.css/,
        loader: "style-loader!css-loader"
      }, {
        test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
        loader: "url-loader"
      }
    ]
  },
  plugins: [HtmlWebpackPluginConfig],
	devServer: {
    historyApiFallback: true,
	}
}
