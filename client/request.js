var Config = require('./config'),
		axios = require('axios'),
		ax = axios.create({
			baseURL: Config.baseUrl,
			headers: Config.requestHeaders
		});

export default ax;
