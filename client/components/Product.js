import React, { Component } from 'react';
import Pixel from "../fbpixel"

import {
	Card, Image, Button
} from 'semantic-ui-react';

export default class Product extends React.Component {

	render() {

		let { image_url, title, description, product_url, _id } = this.props;

		return (
			<Card fluid key={_id}>
				<div style={{ backgroundImage: "url("+image_url+")", width: "100%", height: "220px", backgroundSize: "cover", backgroundPosition: "center 20%" }} />
		    <Card.Content>
		      <Card.Header>
		        {title}
		      </Card.Header>
		      <Card.Description>
		        {description}
		      </Card.Description>
		    </Card.Content>
		    <Card.Content extra textAlign="center">
		      <Button
						size="medium"
						color="orange"
						as="a"
						href={product_url}
						icon="amazon"
						target="_blank"
						content="Auf Amazon ansehen"
						labelPosition="left"
						className="fluidLabelButton"
						style={{ paddingLeft: "1.5em !important" }}
						onClick={() => {
							console.log('Tracking ViewContent');
							Pixel.track('ViewContent', {
								content_ids: [_id],
								contents: product_url,
								content_type: 'product',
								content_name: title
							})
						}}
					/>
		    </Card.Content>
		  </Card>
		)

	}
}
