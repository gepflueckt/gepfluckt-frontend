import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import {
  Segment,
	Container,
  Image,
  Visibility,
	Grid,
	Input,
	Icon,
	Sticky,
	Responsive,
	Button
} from 'semantic-ui-react';

import SideMenu from "./SideMenu";

import { connect } from "react-redux";
import { updateSearchtext } from "../actions/searchActions"
let glue = (state) => {
	return {
		pages: state.pages.pages
	}
}

class TopBar extends React.Component {

	state = {
		mobile: false
	}

	handleUpdate = (e, {width}) => {
		let isMobile = Boolean(width <= 1000);
		this.setState({ mobile: isMobile })
	}

	handleKeydown = (e) => {
		if(e.keyCode == 13) {
			let val = e.target.value;
			this.props.dispatch(updateSearchtext(val));
			// Check if we are on search, if not go to search
			let locationPath = this.props.location.pathname.split("/");
			if(locationPath[1] != "suche")
				window.location.replace('/suche/' + val);
			else
				this.props.history.push('/suche/' + val);
		}
	}

	render() {

		let { toggleSidebar, menuOpen } = this.props;
		let { mobile } = this.state;

		return (
			<Responsive
				as="div"
				fireOnMount
				onUpdate={this.handleUpdate}
			>
				<SideMenu open={menuOpen} />
				<Segment id="topbar" inverted vertical>
					<div className="topbarContainer">
						<div className="logo">
							<a href="/">
								<Image src="/assets/img/Logo-White.svg" height={(mobile) ? "35px" : "50px"} alt="Gepflückt Logo" />
							</a>
						</div>
						<div className="search">
							<Input
								fluid
								icon={<Icon name="search" style={{ top: "-4px" }} />}
								transparent
								inverted
								placeholder={(mobile) ? "Suche..." : "Suche nach tollen Geschenken"}
								size="huge"
								style={{ borderBottom: "2px solid rgba(255,255,255,0.5)", paddingBottom: "5px" }}
								onKeyDown={this.handleKeydown.bind(this)}
							/>
						</div>
						<div className="menuButton">
							<Button
								inverted={!mobile}
								content={(mobile) ? null : "Kategorien"}
								size="small"
								color="orange"
								icon="tags"
								onClick={toggleSidebar}
							/>
						</div>
					</div>
				</Segment>
			</Responsive>
		)

	}
}

export default withRouter(connect(glue)(TopBar));
