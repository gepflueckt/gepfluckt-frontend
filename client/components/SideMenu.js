import React, { Component } from 'react';

import {
  Sidebar,
	Menu
} from 'semantic-ui-react';

import { connect } from "react-redux";
let glue = (state) => {
	return {
		pages: state.pages.pages
	}
}

class SideMenu extends React.Component {

	render() {

		let { open, pages } = this.props;

		let menuItems = [];

		pages.forEach((page) => {
			// Push current page to menu
			menuItems.push(<Menu.Item key={page.menu} as="a" href={page.url} active={true} style={{ paddingLeft: "2em", paddingRight: "2em" }} header>{page.menu}</Menu.Item>)
			// Go through children if applicable
			if(page.children && page.children.length) {
				page.children.forEach((child) => menuItems.push(<Menu.Item as="a" key={page.menu+"-"+child.menu} href={child.url}>{child.menu}</Menu.Item>))
			}
		})

		return (
			<Sidebar
        as={Menu}
        animation='overlay'
        direction='top'
        visible={open}
				id="sideMenu"
				size="huge"
				inverted
				color="orange"
      >
				{ menuItems }
      </Sidebar>
		)

	}
}

export default connect(glue)(SideMenu);
