var Validations = (name, value) => {

	switch (name) {

		case 'nick':
		return { success : true };

		case 'email':
		return { success : true };

		case 'password':
		return { success : true };

		case 'interests':
		return { success : true };

		default:
		return { success : false, message : "Invalid request." };

	}

}

module.exports = (data) => {
	let errors = {};

	for (var name in data) {
		let value = data[name];
		let validation = Validations(name, value);

		if(!validation.success)
			errors[name] = validation.message;
	}

	if (!errors)
		return false;
	else
		return errors;
}
