export default function PageFlatter(pages) {
	let newPages = [];

	pages.forEach((page) => {
		newPages.push({ menu: page.menu, title: page.title, tagId: page.tagId, text: page.text });
    if(page.children) {
    	let children = page.children;
      newPages = [...newPages, ...children]
    }
	})

	return newPages;
}
