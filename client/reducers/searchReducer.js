let initialState = {
	text: '',
	category: '',
	categories: [
		{ title: 'Männer', url: '/maenner',
			children: [
				{ title: 'Schatz', url: '/maenner/schatz' },
				{ title: 'Papa', url: '/maenner/papa' },
				{ title: 'Opa', url: '/maenner/opa' },
			]
		},
		{ title: 'Frauen', url: '/frauen',
			children: [
				{ title: 'Schatz', url: '/frauen/schatz' },
				{ title: 'Papa', url: '/frauen/mama' },
				{ title: 'Opa', url: '/frauen/oma' },
			]
		},
		{ title: 'Teenager', url: '/teens' },
		{ title: 'Kinder', url: '/kinder' },
		{ title: 'Tiere', url: '/tiere',
			children: [
				{ title: 'Hunde', url: '/tiere/hunde' },
				{ title: 'Katzen', url: '/tiere/katzen' }
			]
		}
	]
}

module.exports = (state=initialState, action) => {
	switch(action.type) {
		// Change the search text
		case "UPDATE_SEARCH_TEXT": {
			return {...state, text: action.payload}
			break;
		}
		// Change the search category
		case "UPDATE_SEARCH_CATEGORY": {
			return {...state, category: action.payload}
			break;
		}
	}

	return state;
}
