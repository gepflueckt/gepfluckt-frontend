import React from "react";

let initialState = {
	currentPage: null,
	pages: [
		{
			menu: 'Männer',
			tagId: "5a1ec7f414eb6b17bf260207",
			url: '/männer',
			title: 'Besondere Geschenkideen für Männer',
			text: (
				<p>
					Für Männer ein passendes Geschenk zu finden ist oft gar nicht so einfach. Wir haben daher viele verschiedene besondere Geschenkideen zusammen gestellt, die euren Kumpel, Arbeitskollegen, Papa, Opa, Freund oder Ehemann begeistern wird und direkt auf Amazon erhältlich sind.
				</p>
			),
			children: [
				{
					menu: 'Freund',
					tagId: "5a1ec80e14eb6b17bf26020a",
					title: 'Besondere Geschenkideen für den Freund',
					url: '/freund',
					text: (
						<p>
							Dem Schatz eine Freude zu machen ist es <b>immer</b> wert – und mit dieser Auswahl besonderer Geschenkideen für den Freund fällt es sicher nicht schwer ein tolles Geschenk für den Geliebten zu finden. Einfach eine passende Geschenkidee für den Freund heraussuchen und sofort auf Amazon bestellen.
						</p>
					)
				},
				{
					menu: 'Papa',
					tagId: "5a1ec7fe14eb6b17bf260208",
					title: 'Besondere Geschenkideen für Papa',
					url: '/papa',
					text: (
						<p>
							Oft geben sich Papas mit einfachen Geschenken zufrieden – vielleicht kann man aber dieses Jahr ja mal mit einer besonderen Geschenkidee für Papa den alten Herrn richtig überraschen und ihm eine tolle Freude bereiten. In unserer Selektion von besonderen Geschenkideen findet jeder ein tolles Geschenk für Papa! Einfach direkt auf Amazon shoppen und los geht's.
						</p>
					)
				},
				{
					menu: 'Opa',
					tagId: "5a1ec80714eb6b17bf260209",
					url: '/opa',
					title: 'Besondere Geschenkideen für Opa',
					text: (
						<p>
							Opas bieten Rat, Geborgenheit, Liebe und unendliche Weisheit. Zeigen Sie, wie sehr sie den Großvater schätzen mit unseren besonderen Geschenkideen für Opa. In unserer Selektion von Geschenken findet man ganz einfach das richtige Geschenk, das man sofort und unkompliziert auf Amazon bestellen kann.
						</p>
					)
				}
			]
		},
		{
			menu: 'Frauen',
			tagId: "5a1ec6710efb7b17a8ad2238",
			url: '/frauen',
			title: 'Besondere Geschenkideen für Frauen',
			text: (
				<p>
					Um eine Frau richtig zu begeistern, brauchen Sie besondere Geschenkideen. Verwöhnen Sie die Frauen in Ihrem Leben mal so richtig, egal ob Freundin, Frau, Mama, Oma, Arbeitskollegin oder Tochter. Wir haben für Sie spannende, besondere Geschenkideen zusammen gestellt. Hier findet jeder ein passendes Geschenk für die Frau und kann es sofort über Amazon bestellen.
				</p>
			),
			children: [
				{
					menu: 'Freundin',
					tagId: "5a1ec7ea14eb6b17bf260206",
					url: '/freundin',
					title: 'Besondere Geschenkideen für die Freundin',
					text: (
						<p>
							Auch wenn die Geschenke der alten Schule wie Schmuck und Pralinen immer gut ankommen, mit einer besonderen Geschenkidee für die Freundin kann man seinen Schatz mal so richtig begeistern. Wir haben eine Auslese toller Geschenke für die Geliebte, in der Sie ganz einfach etwas Passendes finden und direkt über Amazon bestellen können.
						</p>
					)
				},
				{
					menu: 'Mama',
					tagId: "5a1ec6770efb7b17a8ad2239",
					url: '/mama',
					title: 'Besondere Geschenkideen für Mama',
					text: (
						<p>
							Mutti ist die Beste – und das sollte man ihr so oft wie möglich zeigen. Wie wäre es denn mit einer besonderen Geschenkidee für Mama? Mit unserer Auswahl an tollen Geschenken findet jeder ein schönes Geschenk, das der Mutter eine Freude bereiten wird. Ganz einfach ein Geschenk aussuchen und sofort über Amazon kaufen.
						</p>
					)
				},
				{
					menu: 'Oma',
					tagId: "5a1ec7e314eb6b17bf260205",
					url: '/oma',
					title: 'Besondere Geschenkideen für Oma',
					text: (
						<p>
							Omas haben ein riesiges Herz, sind unsere privaten Sterneköche aber sind vor allem eins – immer für uns da! Mit diesen besonderen Geschenkideen für Oma können Sie Ihrer Großmutter zeigen, wie viel sie Ihnen bedeutet. Finden Sie ein Geschenk das Ihnen gefällt, können Sie es ganz einfach über Amazon kaufen.
						</p>
					)
				}
			]
		},
		{
			menu: 'Teenager',
			tagId: "5a1ec81c14eb6b17bf26020b",
			url: '/teens',
			title: 'Besondere Geschenkideen für Teenager',
			text: (
				<p>
					Karte mit Geld? <b>Ein Klassiker</b>. Allerdings kann man mit besonderen Geschenkideen für Teenager punkten und den Teens eine Freude machen. Dabei kann man zu witzigen Geschenken greifen oder aber auch etwas praktisches Schenken. Wir haben eine tolle Auswahl toller Geschenke für Sie, die direkt über Amazon bestellt werden können.
				</p>
			) },
		{
			menu: 'Kinder',
			tagId: "5a1ec82214eb6b17bf26020c",
			url: '/kinder',
			title: 'Besondere Geschenkideen für Kinder',
			text: (
				<p>
					Kindern mit tollen Geschenken eine Freude zu machen ist eines der schönsten Dinge, die man im Leben tun kann. Mit unseren besonderen Geschenkideen für Kinder finden Sie das perfekte Geschenk für die Kleinen. Egal ob Kleinkind, Kindergartenkind oder Schulkind, hier finden sie schnell und unkompliziert die besten Geschenkideen für Kinder und können sie direkt auf Amazon kaufen.
				</p>
			) },
		{
			menu: 'Tiere',
			tagId: "5a1ec83514eb6b17bf26020d",
			url: '/tiere',
			title: 'Besondere Geschenkideen für Tiere',
			text: (
				<p>
					Auch Tiere gehören beschenkt! Unsere liebsten Haustiere sind immer für uns da und lieben uns bedingungslos. Na gut, manche unserer pelzigen Freunde sind da ein wenig zurückhaltender, aber jedes Tier hat sich ein tolles Geschenk verdient. Darum haben wir eine Sammlung besonderer Geschenkideen für Tiere erstellt, die Sie ganz unkompliziert über Amazon kaufen können.
				</p>
			),
			children: [
				{
					menu: 'Hunde',
					url: '/hunde',
					title: 'Besondere Geschenkideen für Hunde',
					tagId: "5a1ec83f14eb6b17bf26020e",
					text: (
						<p>
							Dem besten Freund des Menschen mal eine richtige Freude bereiten – mit diesen besonderen Geschenkideen für Hunde wird ihr pelziger Freund garantiert glücklich. Ob Kleinhund oder Labrador, wir haben für jeden Vierbeiner ein passendes Geschenk herausgesucht das Sie sofort und unkompliziert auf Amazon bestellen können.
						</p>
					)
				},
				{
					menu: 'Katzen',
					tagId: "5a1ec84514eb6b17bf26020f",
					url: '/katzen',
					title: 'Besondere Geschenkideen für Katzen',
					text: (
						<p>
							Ob verspielte Mieze oder ruhiger Kater, wir haben eine große Auswahl besonderer Geschenkideen für Katzen herausgesucht. Finden Sie hier ein schönes Geschenk, mit dem auch der eigensinnigste Tiger eine große Freude hat. Einfach direkt auf der Website heraussuchen und sofort auf Amazon shoppen.
						</p>
					)
				}
			]
		}
	]
}

module.exports = (state=initialState, action) => {
	switch(action.type) {
		// Change the current page
		case "SWITCH_PAGE": {
			return {...state, currentPage: action.payload}
			break;
		}
	}

	return state;
}
