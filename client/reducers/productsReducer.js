var _ = require("lodash");

let initialState = {
	fetching: false,
	fetched: false,
	all: []
}

export default function reducer (state=initialState, action) {
	switch(action.type) {
		// Start fetching the products
		case "FETCH_PRODUCTS_START": {
			return {...state, fetching: true}
			break;
		}
		// Products are received
		case "FETCH_PRODUCTS_FULFILLED": {
			return {...state, fetching:false, fetched:true, all:_.shuffle(action.payload)}
			break;
		}
		// Product fetching failed..
		case "FETCH_PRODUCTS_REJECTED": {
			return {...state, fetching:false, error:action.payload}
			break;
		}
	}

	return state;
}
