import { combineReducers } from "redux";

import products from "./productsReducer";
import search from "./searchReducer";
import pages from "./pageReducer";

export default combineReducers({
	products,
	search,
	pages
})
