import ax from '../request.js';

export function fetchProducts() {

	return function (dispatch) {
		dispatch({ type: "FETCH_PRODUCTS_START" });
		console.log('responsing...');
		ax.get("/products")
			.then((response) => {
				dispatch({ type: "FETCH_PRODUCTS_FULFILLED", payload: response.data })
			})
			.catch((err) => {
				dispatch({ type: "FETCH_PRODUCTS_REJECTED", payload: err })
			})
	}

}
