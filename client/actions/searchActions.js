export function updateSearchtext(text) {
	return {
		type: "UPDATE_SEARCH_TEXT",
		payload: text
	}
}

export function selectCategory(category) {
	return {
		type: "UPDATE_SEARCH_CATEGORY",
		payload: category
	}
}
