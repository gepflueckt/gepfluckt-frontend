import React, { Component } from "react";
import { Grid } from "semantic-ui-react";

// Components
import Product from "../components/Product";

export default class ProductListings extends React.Component {

	render() {

		let { products } = this.props;

		let productsList = (
			<h2>Keine Produkte gefunden</h2>
		);

		if (Array.isArray(products)) {
			productsList = products.map((product) => {
				return (
					<Grid.Column key={product._id+"-column"}>
						<Product {...product} />
					</Grid.Column>
				);
			})
		}

		return (
			<Grid doubling stackable columns={4} stretched style={{ paddingBottom: "2em" }}>
				{ productsList }
			</Grid>
		)
	}

}
