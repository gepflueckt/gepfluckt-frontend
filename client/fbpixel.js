import ReactPixel from 'react-facebook-pixel';
import Config from './config.js';
ReactPixel.init(Config.pixelId);

module.exports = ReactPixel;
