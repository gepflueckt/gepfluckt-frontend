import React from "react"
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom"
import { Segment, Button, Sidebar, Responsive, Dimmer, Loader } from "semantic-ui-react";

// Components
import TopBar from "./components/TopBar";
import SideMenu from "./components/SideMenu";

// Containers
import Home from "./pages/Home";
import Search from "./pages/Search";
import Category from "./pages/Category";
import NoMatch from "./pages/NoMatch";
import Impressum from "./pages/Impressum";

// Actions
import { fetchProducts } from "./actions/productsActions";

// Redux
import { connect } from "react-redux";
let glue = (state) => {
	return {
		pages: state.pages.pages,
		fetched: state.products.fetched
	}
}

class App extends React.Component {

	state = {
		menuOpen: false,
		mobile: false
	}

	toggleSidebar = () => {
		this.setState({ menuOpen: !this.state.menuOpen })
	}

	componentWillMount () {
		this.props.dispatch(fetchProducts());
	}

	handleUpdate = (e, {width}) => {
		let isMobile = Boolean(width <= 1000);
		this.setState({ mobile: isMobile })
	}

 	render() {

		let { mobile, menuOpen } = this.state,
				{ pages } = this.props;

		let categoryRoutes = [];

		pages.forEach((page) => {
			// Push current page to menu
			categoryRoutes.push(
				<Route
					key={page.menu}
					exact path={page.url}
					render={(props) => (
						<Category category={page.menu} {...props} mobile={mobile} />
					)}
				/>
			)
			// Children?
			if(page.children && page.children.length) {
				// Go through children and add to routes
				page.children.forEach(
					(child) => {
						categoryRoutes.push(
							<Route
								key={page.menu+"-"+child.menu}
								exact path={child.url}
								render={(props) => (
									<Category category={child.menu} {...props} />
								)}
							/>
						)
					}
				)
			}
		})

    return (
			<Responsive as="div" fireOnMount onUpdate={this.handleUpdate.bind(this)} className="wrapper">
				<Dimmer active={!this.props.fetched}>
					<Loader inverted size="massive" content="Lade Geschenkideen" />
				</Dimmer>
				<TopBar menuOpen={menuOpen} toggleSidebar={this.toggleSidebar.bind(this)} />
				<Switch>
					<Route exact path="/" render={(props) => (<Home {...props} mobile={mobile} />)} />
					{ categoryRoutes }
					<Route exact path="/suche" render={(props) => (<NoMatch {...props} mobile={mobile} />)} status={404} />
					<Route path="/suche/:searchtext" render={(props) => (<Search {...props} mobile={mobile} />)} />
					<Route path="/impressum" render={(props) => (<Impressum {...props} mobile={mobile} />)} />
					<Route render={(props) => (<NoMatch {...props} mobile={mobile} />)} status={404}/>
				</Switch>
				<Segment style={{ marginTop: "5em", textAlign: "right" }} inverted>
					<Button as="a" href="/Impressum" color="orange" flexible basic size="small">Impressum</Button>
				</Segment>
			</Responsive>
    )

  }

}

export default connect(glue)(App)
