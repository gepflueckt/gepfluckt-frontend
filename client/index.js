/*
 * Entry point
 */

import React from 'react';
import {
	BrowserRouter as Router,
	Route,
	Link,
	Switch
} from 'react-router-dom';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";

import Config from "./config";

import Pixel from "./fbpixel";
Pixel.pageView();

import store from "./store";
import App from './App'

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// Reset CSS
//require("./scss/reset.css");
// Load main Styles
import '../semantic/dist/semantic.min.css';
import '../assets/css/styles.css';

ReactDOM.render(
	<Provider store={store}>
		<Router>
			<App />
		</Router>
	</Provider>
	, document.getElementById('root')
);
