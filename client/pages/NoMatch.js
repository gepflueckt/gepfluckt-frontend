import React, { Component } from "react";
import Helmet from "react-helmet";
import {
  Container,
  Grid,
  Header,
  Segment,
	Icon,
	Button
} from "semantic-ui-react";

export default class Home extends React.Component {

 	render() {

 		return (
			<div>
				<Segment
	 				inverted
	 				textAlign="center"
	 				vertical
	 			>
					<Helmet>
						<title>Gepflückt | 404</title>
						<meta name="description" content="Uuups, diese Seite gibt's gar nicht."/>

						<meta property="og:title" content="Gepflückt | 404" />
						<meta property="og:type" content="article" />
						<meta property="og:url" content={"http://gepflückt.de/404/"} />
						<meta property="og:image" content="http://gepflückt.de/assets/img/Social.jpg" />
						<meta property="og:description" content="Uuups, diese Seite gibt's gar nicht." />
					</Helmet>
	 				<Container
						text
						style={{ marginTop: "4em", marginBottom: "4em" }}
						className="firstcontent">
	 					<Header
	 						as="h1"
							textAlign="center"
	 						inverted
							icon
	 					>
							<Header.Content>
						     <Icon name="plug" style={{ marginBottom: "0.5em" }} />
								<Header.Subheader>
									FEHLER 404
								</Header.Subheader>
								Uuups, diese Seite gibt's gar nicht
							</Header.Content>
						</Header>
	 					<p>
							Aber keine Sorge – hier können Sie direkt weiter nach Geschenken suchen:
	 					</p>
						<Button as="a" href="/" color="orange" content="Zur Startseite" />
	 				</Container>
	 			</Segment>
			</div>

 		)
 	}
}
