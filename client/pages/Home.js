import React, { Component } from "react";
import Helmet from "react-helmet";
import {
  Container,
  Grid,
  Header,
  Segment
} from "semantic-ui-react";

import { connect } from "react-redux";
let glue = (state) => {
	return {
		products: state.products.all
	}
}

// Containers
import ProductListings from "../containers/ProductListings";

class Home extends React.Component {

	componentWillMount() {
		document.title = 'Gepflückt';
	}

 	render() {

		let { mobile, products } = this.props;

		let marginSize = (mobile) ? "1em" : "4em";
		let headerSize = (mobile) ? "1.5em" : "2.5em";

 		return (
			<div>
				<Helmet>
					<meta name="description" name="Eine besondere Geschenkidee zu finden fällt oft schwer. Wir haben daher tolle Geschenke für jeden Anlass zusammengestellt. Ob für Freund, Tante, Oma, Mama, Nichte, Enkelin – für jeden wird ein tolles Geschenk schnell gefunden."/>

					<meta property="og:title" content="Gepflückt - besondere Geschenkideen" />
					<meta property="og:type" content="website" />
					<meta property="og:url" content="http://gepflückt.de/" />
					<meta property="og:image" content="http://gepflückt.de/assets/img/Social.jpg" />
					<meta property="og:description" content="Eine besondere Geschenkidee zu finden fällt oft schwer. Wir haben daher tolle Geschenke für jeden Anlass zusammengestellt. Ob für Freund, Tante, Oma, Mama, Nichte, Enkelin – für jeden wird ein tolles Geschenk schnell gefunden." />
			    <title>Gepflückt | Home</title>
				</Helmet>
				<Segment
	 				inverted
	 				textAlign="center"
	 				style={{ minHeight: "60%" }}
	 				vertical
	 				className="christmas"
	 			>
	 				<Container
						text
						style={{ marginTop: marginSize, marginBottom: marginSize }}
						className="firstcontent">
	 					<Header
	 						as="h1"
	 						content="Besondere Geschenkideen für unsere Besten"
	 						inverted
	 						style={{ fontSize: headerSize, fontWeight: "normal" }}
	 					/>
	 					<p>
	 						Eine besondere Geschenkidee zu finden fällt oft schwer. Wir haben daher tolle Geschenke für jeden Anlass zusammengestellt. Ob für Freund, Tante, Oma, Mama, Nichte, Enkelin – für jeden wird ein tolles Geschenk schnell gefunden.
	 					</p>
	 				</Container>
	 			</Segment>
				<Segment basic>
					<Container>
						<Header
	 						as="h2"
	 						content="Beliebte Geschenkideen"
	 						style={{ fontSize: headerSize, paddingBottom: ".5em" }}
	 					/>
						<ProductListings products={products} />
					</Container>
				</Segment>
			</div>
 		)
 	}
}

export default connect(glue)(Home);
