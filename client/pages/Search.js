import React, { Component } from "react";
import {
  Container,
  Grid,
  Header,
  Segment,
	Responsive
} from "semantic-ui-react";
import _ from "lodash";

import { connect } from "react-redux";
let glue = (state) => {
	return {
		products: state.products.all,
		searchtext: state.search.text
	}
}

// Actions
import { updateSearchtext } from "../actions/searchActions";

// Containers
import ProductListings from "../containers/ProductListings";

class Search extends React.Component {

	componentWillMount() {
		if(this.props.match.params.searchtext)
			this.props.dispatch(updateSearchtext(this.props.match.params.searchtext))
	}

 	render() {

		let { products, mobile, searchtext } = this.props;

		let filteredProducts = products.filter((product) => {
			let text = searchtext.toUpperCase();
			let title = product.title.toUpperCase();
			let description = product.description.toUpperCase();
			return _.includes(title, text) || _.includes(description, text);
		})

		let headerSize = (mobile) ? "1.5em" : "3em";

		let resultsHeader = (<span>{filteredProducts.length} Ergebnisse für <i>{searchtext}</i></span>);
		if(!filteredProducts.length)
			resultsHeader = "Keine Ergebnisse gefunden";
		if(searchtext == "")
			resultsHeader = "Alle Produkte";

 		return (
			<div>
				<Segment
	 				inverted
	 				textAlign="center"
	 				vertical
	 			>
	 				<Container
						text
						style={{ marginTop: "1em", marginBottom: "1em" }}
						className="firstcontent">
	 					<Header
	 						as="h1"
	 						content="Suchergebnisse"
	 						inverted
	 						style={{ fontSize: headerSize, fontWeight: "normal" }}
	 					/>
	 				</Container>
	 			</Segment>
				<Segment basic>
					<Container>
						<Header
	 						as="h2"
	 						content={resultsHeader}
	 						style={{ fontSize: "2em", paddingBottom: ".5em" }}
	 					/>
						<ProductListings products={filteredProducts} />
					</Container>
				</Segment>
			</div>
 		)
 	}
}

export default connect(glue)(Search);
