import React, { Component } from "react";
import Helmet from "react-helmet";
import {
  Container,
  Grid,
  Header,
  Segment
} from "semantic-ui-react";
import _ from "lodash";

// Containers
import ProductListings from "../containers/ProductListings";

// Helpers
import PageFlatter from "../helpers/PageFlatter";

import { connect } from "react-redux";
let glue = (state) => {
	return {
		products: state.products.all,
		pages: state.pages.pages
	}
}

class Category extends React.Component {

	state = {
		page: null
	}

	componentWillMount() {
		let { pages, category } = this.props;
		let flatpages = PageFlatter(pages);
		let page = flatpages.find((el) => { return el.menu == category });
		this.setState({ page: page })
	}

 	render() {

		let { products, pages, mobile } = this.props;
		let { menu, title, text, tagId } = this.state.page;

		// Products
		let catProducts = products.filter((el) => { return _.includes(el.tags, tagId); });

		let marginSize = (mobile) ? "1em" : "4em";
		let headerSize = (mobile) ? "1.5em" : "2.5em";

 		return (
			<div>
				<Helmet>
					<title>Gepflückt | {menu}</title>
					<meta name="description" content={title+ " | "+text}/>

					<meta property="og:title" content={"Gepflückt | "+menu} />
					<meta property="og:type" content="article" />
					<meta property="og:url" content={"http://gepflückt.de/"+menu+"/"} />
					<meta property="og:image" content="http://gepflückt.de/assets/img/Social.jpg" />
					<meta property="og:description" content="Gepflückt - besondere Geschenkideen. Das Impressum: Verantwortlich für den Inhalt ist Mike Hudson." />
				</Helmet>
				<Segment
	 				inverted
	 				textAlign="center"
	 				style={{ minHeight: "60%" }}
	 				vertical
	 				className="christmas"
	 			>
	 				<Container
						text
						style={{ marginTop: marginSize, marginBottom: marginSize }}
						className="firstcontent">
	 					<Header
	 						as="h1"
	 						content={title}
	 						inverted
	 						style={{ fontSize: headerSize, fontWeight: "normal" }}
	 					/>
						{text}
	 				</Container>
	 			</Segment>
				<Segment basic>
					<Container>
						<Header
	 						as="h2"
	 						content={ "Amazon Geschenke – " + menu }
	 						style={{ fontSize: headerSize, paddingBottom: ".5em" }}
	 					/>
						<ProductListings products={catProducts} />
					</Container>
				</Segment>
			</div>

 		)
 	}
}

export default connect(glue)(Category)
